class NegociacaoController {
    
    constructor(){

        let $ = document.querySelector.bind(document);
        
        this._ordemAtual = '';

        this._inputData = $('#data');
        this._inputQuantidade = $('#quantidade');
        this._inputValor = $('#valor');
        
        this._listaNegociacoes = new Bind(
            new ListaNegociacoes(),
            new NegociacoesView($('#negociacoesView')),
            'adiciona', 'limpar', 'ordena', 'inverterOrdem');

        this._mensagem = new Bind(
            new Mensagem(), new MensagemView($('#mensagemView')),
            'texto');
    }
    

    adiciona(event) {

        event.preventDefault();

        try {
            
            this._listaNegociacoes.adiciona(this._criaNegociacao());
            this._mensagem.texto = "Negociação adicionada com sucesso";
            this._limpaFormulario();
        } catch(erro) {
            this._mensagem.texto = erro;
        }
    }

    dataString(data) {

      return  data.split('-')
            .map((item, indice) => {

                return item - indice % 2
            })
    }

    _criaNegociacao(){

        return new Negociacao(
            this.inputData,
            this.inputQuantidade,
            this.inputVAlor 
        );
    }

    _limpaFormulario(){

        this._inputData.value = "";
        this._inputQuantidade.value = 1;
        this._inputValor.value = 0;

        this._inputData.focus();
    }

    
    getNumeros(numeros) {
        
        return numeros
            .map((item) => {

            return item * 2;
        })
    }

    get inputData(){

        return DateHelper.textoParaData(this._inputData.value);
    }

    get inputQuantidade(){

        return this._inputQuantidade.value;
    }

    get inputVAlor(){

        return this._inputValor.value;
    }

    get volume(){

        return this._inputQuantidade.value * this._inputValor.value;
    }

    apagar(){

        this._listaNegociacoes.limpar();
        this._mensagem.texto = "Negociações apagadas com sucesso.";
    }

    importaNegociacoes(){

        let service = new NegociacaoService();

        Promise.all([
            service.obterNegociacoesDaSemana(),
            service.obterNegociacoesAnterior(),
            service.obterNegociacoesRetrasada()
        ]).then(negociacoes => {
            negociacoes
                .reduce((arrayAchatado, array) => arrayAchatado.concat(array), [])
                .forEach(negociacao => this._listaNegociacoes.adiciona(negociacao));
            this._mensagem.texto = 'Negociacões importadas com sucesso';
        })
        .catch(err => this._mensagem.texto = err);
    }

    ordena(coluna){

        if(this._ordemAtual == coluna){

            this._listaNegociacoes.inverterOrdem();
        } else {

            this._listaNegociacoes.ordena((a,b) => a[coluna] - b[coluna]);
        }
        this._ordemAtual = coluna;

    }
}