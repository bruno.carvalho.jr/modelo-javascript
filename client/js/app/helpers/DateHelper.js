class DateHelper {

    constructor(){

        throw new Error('DateHelper não pode ser instanciada');
    }

   static textoParaData(texto) {

        if(!/\d{2}\/\d{2}\/\d{4}/.test(texto))
            throw new Error('A Data deve estar no formato dd/mm/aaaa.');

        return  new Date(...texto.split('/')
            .reverse()
            .map((item, indice) => {

                return item - indice % 2
            }))
    }

    static dataParaTexto(data) {

        return `${data.getDate()}/${data.getMonth()+1}/${data.getFullYear()}`;   
    }
}
