class ListaNegociacoes {

    constructor( armadilha){

        this._negociacoes = [];
        this._armadilha = armadilha;
    }

    adiciona(negociacao) {

        this._negociacoes.push(negociacao);
    }

    get negociacoes() {

        //programação defenciva
        return [].concat(this._negociacoes);
    }

    limpar() {

        this._negociacoes = [];
    }

    get volumeTotal() {

        return this._negociacoes.reduce((total, n) => total + n.volume, 0.0)
    }

    ordena(criterio) {

        this._negociacoes.sort(criterio);
    }

    inverterOrdem() {

        this._negociacoes.reverse();
    }
}