class Negociacao{

    constructor(data, quantidade, valor) {

        let dat = new Date(data.getTime());
        this._data = dat.getDate() + '/' + (dat.getMonth() + 1) + '/' + dat.getFullYear();
        this._quantidade = quantidade;
        this._valor = valor;
        Object.freeze(this);
    }

    get volume(){
        return this._quantidade * this._valor;
    }

    get data(){

        return this._data;
    }

    get quantidade(){

        return this._quantidade;
    }

    get valor(){

        return this._valor;
    }


}